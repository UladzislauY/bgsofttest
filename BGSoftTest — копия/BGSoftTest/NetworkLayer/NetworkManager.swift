//
//  NetworkManager.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

protocol Networking {
    
    func requestPhotos(from urlString: String, executing: @escaping (PhotoDictionary) -> ()) throws
}

final class NetworkManager: Networking {
    
    func requestPhotos(from urlString: String, executing: @escaping (PhotoDictionary) -> ()) throws {
        guard let url = URL(string: urlString) else {throw ErrorHandler.incorrectUrl}
        let dataTask = createDataTask(with: url, completion: executing)
        dataTask?.resume()
    }
    
    private func createDataTask(with url: URL, completion: @escaping (PhotoDictionary) ->()) -> URLSessionDataTask? {
        return URLSession.shared.dataTask(with: url) { (data, _, _) in
            guard let data = data else {return}
            guard let photos = try? JSONDecoder().decode(PhotoDictionary.self, from: data) else {return}
            DispatchQueue.main.async {
                completion(photos)
            }
        }
    }
}
