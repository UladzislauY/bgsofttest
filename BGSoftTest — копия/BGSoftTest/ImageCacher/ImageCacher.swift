//
//  ImageCacher.swift
//  BGSoftTest
//
//  Created by Владислав on 12/25/21.
//

import UIKit


class ImageCacher {
    
    static let shared = ImageCacher()
    private init() {}
    
    private var cache = NSCache<NSString, UIImage>()
    
    func cacheImage(from url: URL, completion: (UIImage) -> ()) {
        if let cachedImage = cache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        }
    }
    
    func saveImage(from url: URL, image: UIImage) {
        cache.setObject(image, forKey: url.absoluteString as NSString)
    }
}
