//
//  MainBuilder.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

protocol MainBuilderProtocol {
    
    func createPhotoGalleryScene() -> UIViewController
}

final class MainBuilder: MainBuilderProtocol {
    
    func createPhotoGalleryScene() -> UIViewController {
        let photoGalleryScene = PhotoGalleryViewController()
        let networkManager = NetworkManager()
        let presenter = PhotoGalleryViewMainPresenter(view: photoGalleryScene, networkManager: networkManager)
        photoGalleryScene.presenter = presenter
        return photoGalleryScene
    }
}
