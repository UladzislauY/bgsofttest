//
//  Model.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

typealias PhotoDictionary = [String: Photo]

struct Photo: Codable {
    
    let photoURL, userURL, userName: String
    let colors: [String]
    
    enum CodingKeys: String, CodingKey {
        case photoURL = "photo_url"
        case userURL = "user_url"
        case userName = "user_name"
        case colors
    }
}
