//
//  ViewController.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

enum LayoutPresentationStyle: String, CaseIterable {
    case horizontal
    case column
}

final class PhotoGalleryViewController: UIViewController {
    
    private enum Constant {
        static let numberOfItems: CGFloat = 2
        static let widthPadding: CGFloat = 100
        static let heightPadding: CGFloat = 300
    }
    //MARK: - UserInterface Elements
    private lazy var photoGalleryCollectionView = PhotoGalleryCollectionView(layout: ZoomAndSnapFlowLayout(itemWidth: self.view.bounds.width - Constant.widthPadding / Constant.numberOfItems, itemHeight: self.view.bounds.height - Constant.heightPadding / Constant.numberOfItems), dataSource: self, delegate: self, backgroundColor: .white, cell: PhotoGalleryCollectionViewCell.self, cellIdentifier: PhotoGalleryCollectionViewCell.reuseIdentifier)
    //MARK: - Variables
    public var presenter: PhotoGalleryViewPresenterProtocol!
    private var presentationStyle: LayoutPresentationStyle = .horizontal {
        didSet {updatePresentationStyle()}
    }
    private var layoutStyleDelegate: [LayoutPresentationStyle: UICollectionViewDelegateFlowLayout] = {
        let layouts: [LayoutPresentationStyle: UICollectionViewDelegateFlowLayout] = [.column: ColumnCollectionViewLayout()]
        return layouts
    }()
    
    //MARK: - ViewController Life Cycle Functions 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(photoGalleryCollectionView)
        retrievePhotos()
        addPinchInOutGesture()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.photoGalleryCollectionView.frame = self.view.bounds
        self.photoGalleryCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    @objc private func pinchInOutGesture(_ gesture: UIPinchGestureRecognizer) {
        presenter.setGalleryPresentationStyle(with: presentationStyle) { [weak self] (style) in
            self?.presentationStyle = style
        }
        if gesture.state == .changed {
            
            updatePresentationStyle()
        }
    }
    
    private func addPinchInOutGesture() {
        let pinchGesture: UIGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinchInOutGesture(_:)))
        photoGalleryCollectionView.addGestureRecognizer(pinchGesture)
    }
    
    private func updatePresentationStyle() {
        photoGalleryCollectionView.delegate = layoutStyleDelegate[presentationStyle]
        photoGalleryCollectionView.performBatchUpdates({
//            photoGalleryCollectionView.setCollectionViewLayout(ColumnCollectionViewLayout(), animated: true)
            reloadPhotoGalleryData()
        })
    }
}

//MARK: - PhotoGalleryViewController Extension
extension PhotoGalleryViewController: PhotoGalleryViewProtocol {
    func reloadPhotoGalleryData() {
        DispatchQueue.main.async {
            self.photoGalleryCollectionView.reloadData()
        }
    }
    
    func retrievePhotos() {
        presenter.fetchPhotos { [weak self] (photos) in
            guard let self = self else {return}
            self.presenter.photos = photos
            self.reloadPhotoGalleryData()
        }
    }
}
//MARK: - UICollectionView Extension
extension PhotoGalleryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return presenter.filterPhotosAlphabetically().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let photoGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoGalleryCollectionViewCell.reuseIdentifier, for: indexPath) as? PhotoGalleryCollectionViewCell else {return UICollectionViewCell()}
        photoGalleryCell.populatePhotoGalleryCollectionViewCell(using: presenter.getPhotoInformation(at: indexPath.item).key, with: presenter.filterPhotosAlphabetically()[indexPath.item])
        return photoGalleryCell
    }
}

extension PhotoGalleryViewController: UICollectionViewDelegate {}
