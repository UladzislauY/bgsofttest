//
//  PhotoGalleryCollectionViewCell.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

protocol PhotoGalleryCollectionViewCellDelegate {
    
    func populatePhotoGalleryCollectionViewCell(using userKey: String, with object: Photo?)
}

final class PhotoGalleryCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Constants
    private enum Constant {
        static let photoImageViewTopAnchor: CGFloat = 100
        static let photoImageViewBottomAnchor: CGFloat = -100
        static let photoImageViewLeftAnchor: CGFloat = 40
        static let photoImageViewRightAnchor: CGFloat = -40
        static let imageExtension: String = ".jpg"
        static let photoImageViewCornerRadius: CGFloat = 16
        static let userNameLabelFontSize: CGFloat = 10
        static let userNameLabelFontName: String = "Helvetica"
        static let baseURL: String = "http://dev.bgsoft.biz/task/"
    }
    
    //MARK: - UserInterface Elements
    private lazy var photoImageView: UIImageView  = {
        let photo = UIImageView()
        photo.applyShadowWithCornerRadius(containerView: containerView, cornerRadius: Constant.photoImageViewCornerRadius, contentMode: .scaleToFill)
        return photo
    }()
    
    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: Constant.userNameLabelFontName, size: Constant.userNameLabelFontSize)
        label.textColor = .white
        return label
    }()
    
    private lazy var containerView: UIView = {
        let container = UIView()
        return container
    }()
    
    //MARK: - Variables
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    //MARK: - Cell Life Cycle Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.photoImageView.image = nil
        self.userNameLabel.text = nil 
    }
    
    //MARK: - Layout setup
    private func setup() {
        contentView.addSubview(containerView)
        containerView.addSubview(photoImageView)
        photoImageView.addSubview(userNameLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.activateConstraintsOnView(top: contentView.safeAreaLayoutGuide.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.safeAreaLayoutGuide.bottomAnchor, trailing: contentView.trailingAnchor, padding: .init(top: Constant.photoImageViewTopAnchor, left: Constant.photoImageViewLeftAnchor, bottom: Constant.photoImageViewBottomAnchor, right: Constant.photoImageViewRightAnchor), size: .init())
        
        photoImageView.frame = containerView.bounds
        
        userNameLabel.activateConstraintsOnView(top: nil, leading: photoImageView.leadingAnchor, bottom: photoImageView.bottomAnchor, trailing: photoImageView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 50))
        
    }

}

//MARK: - Extension
extension PhotoGalleryCollectionViewCell: PhotoGalleryCollectionViewCellDelegate {
    func populatePhotoGalleryCollectionViewCell(using userKey: String, with object: Photo?) {
        guard let photos = object else {return}
        self.userNameLabel.text = photos.userName.capitalized
        self.photoImageView.loadImage(from: "\(Constant.baseURL)\(userKey)\(Constant.imageExtension)") { [weak self] (photoImage) in
            self?.photoImageView.image = photoImage
        }
        for color in photos.colors {
            guard let mix = UIColor(hex: color) else {return}
            self.photoImageView.applyGradient(using: [mix], type: .radial)
        }
    }
}
