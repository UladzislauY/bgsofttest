//
//  PhotoGalleryCollectionView.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

final class PhotoGalleryCollectionView: UICollectionView {
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(layout: ZoomAndSnapFlowLayout, dataSource: UICollectionViewDataSource, delegate: UICollectionViewDelegate, backgroundColor: UIColor, cell: UICollectionViewCell.Type, cellIdentifier: String) {
        self.init(frame: .zero, collectionViewLayout: ZoomAndSnapFlowLayout())
        super.collectionViewLayout = layout
        super.dataSource = dataSource
        super.delegate = delegate
        super.backgroundColor = backgroundColor
        super.register(cell, forCellWithReuseIdentifier: cellIdentifier)
        
    }
    
}
