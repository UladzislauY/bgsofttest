//
//  ColumnCollectionViewLayout.swift
//  BGSoftTest
//
//  Created by Владислав on 12/26/21.
//

import UIKit


class ColumnCollectionViewLayout: UICollectionViewFlowLayout, UICollectionViewDelegateFlowLayout {
    
    private let itemsPerRow: CGFloat = 3
    private let minimumItemSpace: CGFloat = 8
    private let minimumLineSpace: CGFloat = 10
    private let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: -10.0, right: -10.0)
    
    override init() {
        super.init()
        scrollDirection = .vertical
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var countCells = 2
        var offset: CGFloat = 2.0
        let frameCV = collectionView.frame
        let widthCell = frameCV.width / CGFloat(countCells)
        let heightCell = widthCell
        let spacing = CGFloat(countCells + 1) * offset / CGFloat(countCells)
        return CGSize(width: widthCell - spacing, height: heightCell - (offset*2))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumItemSpace
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
