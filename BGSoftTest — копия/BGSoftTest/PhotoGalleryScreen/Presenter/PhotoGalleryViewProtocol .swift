//
//  PhotoGalleryViewProtocol .swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

protocol PhotoGalleryViewProtocol: AnyObject {
    
    func retrievePhotos()
    func reloadPhotoGalleryData()
}
