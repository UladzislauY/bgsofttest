//
//  PhotoGalleryViewMainPresenter.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

final class PhotoGalleryViewMainPresenter: PhotoGalleryViewPresenterProtocol {
    
    enum Constant {
        static let baseURL = "http://dev.bgsoft.biz/task/"
        static let endpoint = "credits.json"
        
        static var photosURL: String {
            return baseURL.appending(endpoint)
        }
    }
    
    weak var view: PhotoGalleryViewProtocol?
    private let networkManager: Networking
    public var photos = PhotoDictionary()
    
    init(view: PhotoGalleryViewProtocol, networkManager: Networking) {
        self.view = view
        self.networkManager = networkManager
    }
    
    func fetchPhotos(completion: @escaping (PhotoDictionary) -> ())  {
        try? networkManager.requestPhotos(from: Constant.photosURL, executing: {  (photos) in
            completion(photos)
        })
    }
    
    func getPhotoInformation(at index: Int) -> (key: String, value: Photo) {
        let photoInformation = Array(photos)[index]
        return (photoInformation.key, photoInformation.value)
    }
    
    func removePhotoFromGallery(at index: Int)  {
        var photoList = Array(photos)
        photoList.remove(at: index)
    }
    
    func filterPhotosAlphabetically() -> [Photo] {
        let photoValues = self.photos.values
        let filteredPhotos = photoValues.sorted { (lhs, rhs) -> Bool in
            return  lhs.userName < rhs.userName
        }
        return filteredPhotos
    }
    
    func setGalleryPresentationStyle(with style: LayoutPresentationStyle, completion: (LayoutPresentationStyle) -> ()) {
        let allStyles = LayoutPresentationStyle.allCases
        guard let index = allStyles.firstIndex(of: style) else {return}
        let nextIStyle = (index + 1) % allStyles.count
        completion(allStyles[nextIStyle])
    }
}
