//
//  PhotoGalleryViewPresenterProtocol.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

protocol PhotoGalleryViewPresenterProtocol {
    
    func setGalleryPresentationStyle(with style: LayoutPresentationStyle, completion: (LayoutPresentationStyle)->())
    func filterPhotosAlphabetically() -> [Photo]
    func removePhotoFromGallery(at index: Int)
    func getPhotoInformation(at index: Int) -> (key: String, value: Photo)
    func fetchPhotos(completion: @escaping (PhotoDictionary) ->())
    var photos: PhotoDictionary {get set}
}
