//
//  UIView Extension .swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

fileprivate enum ViewConstant {
    
    static let shadowRadius: CGFloat = 10
    static let containerCornerRadius: CGFloat = 10
    static let shadowOpacity: Float = 10
    static let shadowWidth: CGFloat = 1.0
    static let shadowHeight: CGFloat = 1.0
}

extension UIView {
    
    func anchorSize(to view: UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func activateConstraintsOnView(top: NSLayoutYAxisAnchor?,
                                   leading: NSLayoutXAxisAnchor?,
                                   bottom: NSLayoutYAxisAnchor?,
                                   trailing: NSLayoutXAxisAnchor?,
                                   padding: UIEdgeInsets = .zero,
                                   size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
            
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        
    }
    
    func applyShadowWithCornerRadius(containerView : UIView, cornerRadius : CGFloat, contentMode: ContentMode){
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = ViewConstant.shadowOpacity
        containerView.layer.shadowOffset = CGSize.init(width: ViewConstant.shadowWidth, height: ViewConstant.shadowHeight)
        containerView.layer.shadowRadius = ViewConstant.shadowRadius
        containerView.layer.cornerRadius = ViewConstant.containerCornerRadius
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.contentMode = contentMode
        containerView.addSubview(self)
        
    }
    
    func applyBlurEffect()  {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(blurEffectView)
    }
}
