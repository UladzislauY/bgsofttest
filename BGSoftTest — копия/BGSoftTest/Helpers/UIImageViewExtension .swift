//
//  UIImageViewExtension .swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import UIKit

fileprivate enum ImageConstant {
    
    static let percentage: CGFloat = 0.1
    static var stringFromURL: String?
}

extension UIImageView {
    
    func loadImage(from urlString: String, completion: @escaping (UIImage) ->())  {
        ImageConstant.stringFromURL = urlString
        image = nil
        guard let url = URL(string: urlString) else {return}
        ImageCacher.shared.cacheImage(from: url) { (cachedImage) in image = cachedImage}
        DispatchQueue.global(qos: .background).async {
            guard let data = try? Data(contentsOf: url) else {return}
            guard let imageToCache = UIImage(data: data)?.resized(withPercentage: ImageConstant.percentage) else {return}
            DispatchQueue.main.async {
                if ImageConstant.stringFromURL == urlString {completion(imageToCache)}
            }
            ImageCacher.shared.saveImage(from: url, image: imageToCache)
        }
    }
    
    func applyGradient(using colors: [UIColor], type: CAGradientLayerType) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.type = type
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 1, y: 1)
        let endY = 0.5 + self.frame.size.width / self.frame.size.height / 2
        gradientLayer.endPoint = CGPoint(x: 1, y: endY)
        self.layer.frame = self.bounds
        self.layer.addSublayer(gradientLayer)
    }
}
