//
//  Dictionary Extension.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

extension Dictionary {
    
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }
}
