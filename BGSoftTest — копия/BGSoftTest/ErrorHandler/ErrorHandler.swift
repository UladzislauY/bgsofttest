//
//  ErrorHandler.swift
//  BGSoftTest
//
//  Created by Владислав on 12/23/21.
//

import Foundation

enum ErrorHandler: String, Error {
    case incorrectUrl = "Unable to retrieve data from remote server due to incorrect url"
    case incorrectDataFormat = "Unable to retrieve data from remote server due to incorrect data format"
}
